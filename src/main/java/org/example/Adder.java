package org.example;

public final class Adder {
    public int add(int first, int second) {
        return first + second;
    }
}
