package org.registration;

public final class UserProfile {

    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;
    private String email;
    private int height; // in cm
    private double weight; // in kg
    private String bodyType;
    private String goalType;


    public UserProfile() {
        this.firstName = "";
        this.lastName = "";
        this.age = 0;
        this.gender = null;
        this.email = "";
        this.height = 0;
        this.weight = 0;
        this.bodyType = "";
        this.goalType = "";
    }
// TO DO
    // активност, преференции диета, регион, очаквано тегло, никнейм, парола
    // насоки калории


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String s) {
        firstName = s;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String s) {
        lastName = s;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int a) {
        age = a;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender g) {
        gender = g;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String s) {
        email = s;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int h) {
        height = h;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double w) {
        weight = w;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String s) {
        bodyType = s;
    }

    public String getGoalType() {
        return goalType;
    }

    public void setGoalType(String s) {
        goalType = s;
    }


    public String genderToString() {

        if (getGender() == Gender.Man) {
            if (getAge() < 18) {
                return "Boy";
            } else {
                return "Man";
            }
        }
        if (getGender() == Gender.Woman) {
            if (getAge() < 18) {
                return "Girl";
            } else {
                return "Woman";
            }
        }

        return "";
    }

    public String greet() {
        String greeting = "! Welcome to our program !" + "\n"
                + getFirstName() + " " + getLastName() + "\n"
                + "Age: " + getAge() + ", Gender: "
                + genderToString()
                + ", Height: " + getHeight() + "cm ,"
                + "Weight: " + getWeight() + "kg \n"
                + "BodyType: " + getBodyType() + ", Goal:"
                + getGoalType() + "\n" + "email: " + getEmail() + "\n";
        return greeting;
    }


}
