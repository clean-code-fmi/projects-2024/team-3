package org.registration;

import java.io.InputStream;
import java.util.Scanner;


public class UserMenu {

    private Scanner scanner;

    public UserMenu(InputStream iStr) {
        scanner = new Scanner(iStr);
    }
    private UserProfile userProfileDataAssigning(String firstName,
                                    String lastName,
                                    int age, Gender gender, String email,
                                    int height, double weight,
                                    String bodyType, String goalType) {

        UserProfile usrProf = new UserProfile();
        usrProf.setFirstName(firstName);
        usrProf.setLastName(lastName);
        usrProf.setAge(age);
        usrProf.setGender(gender);
        usrProf.setEmail(email);
        usrProf.setHeight(height);
        usrProf.setWeight(weight);
        usrProf.setBodyType(bodyType);
        usrProf.setGoalType(goalType);

        System.out.print(usrProf.greet());
        return usrProf;
    }
    public final UserProfile register() {
        System.out.print("Enter your first name: ");
        String fName = scanner.nextLine();

        System.out.print("Enter your last name: ");
        String lName = scanner.nextLine();

        System.out.print("Enter your age: ");
        int age = scanner.nextInt();

        Gender gender = null;

        do {
            System.out.print("Enter your gender, 0 for women and 1 for men: ");

            int gendernumber = scanner.nextInt();

            if (gendernumber == 0) {
                gender = Gender.Woman;
            }
            if (gendernumber == 1) {
                gender = Gender.Man;
            }
        }
        while (gender == null);

        scanner.nextLine(); // for some reason after reading a
                            // number with nextInt, nextLine gets
                            // the '\n' not the actual next string
                            // this nextLine is for capturing
                            // this unwanted behaviour
        System.out.print("Enter your email: ");
        String email = scanner.nextLine();

        System.out.print("Enter your height: ");
        int height = scanner.nextInt();

        System.out.print("Enter your weight: ");
        double weight = scanner.nextDouble();

        scanner.nextLine(); // for some reason after reading a
                            // number with nextInt, nextLine gets
                            // the '\n' not the actual next string
                            // this nextLine is for capturing this
                            // unwanted behaviour

        System.out.print("Enter your body type: ");
        // for now only customer set body type, not options from me
        String bodyType = scanner.nextLine();

        System.out.print("Enter your goal: ");
        // for now only customer set goal, not options from me
        String goalType = scanner.nextLine();

        return userProfileDataAssigning(fName, lName, age, gender,
                        email, height, weight,
                        bodyType, goalType);
    }
}
