package app.cli.handler;

import app.logging.water.WaterDiary;

import java.time.LocalDateTime;
import java.util.Scanner;

public final class WaterDiaryLogHandler implements CommandHandler {
    private final WaterDiary waterDiary;
    private final Scanner scanner;

    public WaterDiaryLogHandler(WaterDiary waterDiary, Scanner scanner) {
        this.scanner = scanner;
        this.waterDiary = waterDiary;
    }
    @Override
    public void execute() {
        System.out.println("Enter amount of water in ml. to log:");
        try {
            logWater();
            System.out.println("Water was logged successfully!");
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Please enter a numeric value.");
        }
    }

    @Override
    public String getDescription() {
        return "Allows the user to log their water intake.";
    }

    private void logWater() {
        System.out.print("> ");
        double amount = Double.parseDouble(scanner.nextLine());
        LocalDateTime currentTime = LocalDateTime.now();
        waterDiary.logWater(currentTime, amount);
    }
}
