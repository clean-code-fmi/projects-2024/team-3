package app.cli.handler;
public interface CommandHandler {
    void execute();

    String getDescription();
}
