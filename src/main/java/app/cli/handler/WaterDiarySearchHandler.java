package app.cli.handler;

import app.logging.date.DateConverter;
import app.logging.water.Water;
import app.logging.water.WaterDiary;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.SortedMap;

public final class WaterDiarySearchHandler implements CommandHandler {
    private final Scanner scanner;
    private final WaterDiary waterDiary;

    public WaterDiarySearchHandler(WaterDiary waterDiary, Scanner scanner) {
        this.waterDiary = waterDiary;
        this.scanner = scanner;
    }

    @Override
    public void execute() {
        System.out.println("Would you like to search by date or "
                + "by timeframe of specific date?\n"
                + "Enter \"date\" or \"timeframe\" for your choice: ");
        String choice = extractChoice(scanner);
        try {
            switch (choice) {
                case "date":
                    findByDate(scanner);
                    break;
                case "timeframe":
                    findByTimeFrame(scanner);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid input. Please enter a valid choice. "
                    + "\n\"date\" or \"timeframe\"");
        } catch (Exception e) {
            System.out.println("Unexpected error occurred. Please try again.");
        }
    }

    @Override
    public String getDescription() {
        return "Allows you to search logs in your water diary.";
    }

    private String extractChoice(Scanner scanner) {
        System.out.print("> ");
        return scanner.nextLine().toLowerCase();
    }

    private void findByDate(Scanner scanner) {
        System.out.println("Please enter date in the format DD-MM-YYYY.");
        try {
            String dateInput = extractChoice(scanner);
            LocalDateTime date = DateConverter.
                    convert(dateInput);
            SortedMap<LocalDateTime, Water> entries = waterDiary.
                    getEntriesByDate(date);
            printEntries(entries);
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
    }

    private void findByTimeFrame(Scanner scanner) {
        System.out.println("Please enter two timestamps in the format "
                + "DD-MM-YYYY HH:mm.");
        try {
            System.out.print("From: ");
            String timestampString = extractChoice(scanner);
            LocalDateTime timestampFrom = DateConverter.
                    convert(timestampString);
            System.out.print("To: ");
            timestampString = extractChoice(scanner);
            LocalDateTime timestampTo = DateConverter.
                    convert(timestampString);
            SortedMap<LocalDateTime, Water> entries = waterDiary.
                    getEntriesByTimeFrame(timestampFrom, timestampTo);
            printEntries(entries);
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
    }

    private void printEntries(SortedMap<LocalDateTime, Water> entries) {
        DateTimeFormatter formatter = DateTimeFormatter.
                ofPattern("dd-MM-yyyy HH:mm");
        for (Map.Entry<LocalDateTime, Water> entry : entries.entrySet()) {
            String formattedDate = entry.getKey().format(formatter);
            System.out.println(formattedDate + ": " + entry.getValue().
                    toString());
        }
    }
}
