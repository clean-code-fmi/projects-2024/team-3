package app.cli;

import app.cli.handler.CommandHandler;
import java.util.Scanner;

public final class CommandLineInterface {
    private boolean running;
    private Scanner scanner;
    private String command;
    private final CommandRegistry menuCommandRegistry;
    public CommandLineInterface(CommandRegistry registry, Scanner scanner) {
        this.running = true;
        this.scanner = scanner;
        this.menuCommandRegistry = registry;
    }

    public void run() {
        greet();
        while (running) {
            extractCommand();
            try {
                switch (command) {
                    case "exit":
                        exit();
                        break;
                    case "help":
                        help();
                        break;
                    default:
                        CommandHandler commandHandler =
                                menuCommandRegistry.getHandler(command);
                        commandHandler.execute();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void greet() {
        System.out.println("Welcome! Please register or log in.");
    }

    private void extractCommand() {
        System.out.print("> ");
        command = scanner.nextLine().toLowerCase();
    }

    private void help() {
        menuCommandRegistry.printSupportedCommands();
    }

    private void exit() {
        System.out.println("Goodbye!");
        running = false;
    }

}
