package app.cli;
import app.cli.handler.CommandHandler;
import java.util.Map;

public final class CommandRegistry {


    private final Map<String, CommandHandler> supportedCommands;

    public CommandRegistry(final Map<String,
            CommandHandler> supportedCommands) {
        this.supportedCommands = supportedCommands;
    }

    public CommandHandler getHandler(String command) {
        if (!supportedCommands.containsKey(command)) {
            throw new IllegalArgumentException("Unknown command: " + command
                    + "\nPlease use the command \"help\" "
                    + "to see currently supported commands!");
        }
        return supportedCommands.get(command);
    }

    public void printSupportedCommands() {
        System.out.println("Currently supported:");
        supportedCommands.forEach((commandName, command) -> {
            String commandDescription = command.getDescription();
            System.out.println(commandName + ": " + commandDescription);
        });
        System.out.println("help: Lists currently supported functionality");
        System.out.println("exit: Exits the program");
    }
}
