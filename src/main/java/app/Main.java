package app;

import app.cli.CommandLineInterface;
import app.cli.CommandRegistry;
import app.cli.handler.CommandHandler;
import app.cli.handler.WaterDiaryLogHandler;
import app.cli.handler.WaterDiarySearchHandler;
import app.logging.EntryStorage;
import app.logging.water.WaterDiary;

import java.util.Map;
import java.util.Scanner;

public final class Main {
    //solves checkstyle "no def constructor for util classes" bs
    private Main() { }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final Map<String, CommandHandler> menuSupportedCommands =
                initializeHandlers(scanner);
        final CommandRegistry menuCommandRegistry =
                new CommandRegistry(menuSupportedCommands);
        CommandLineInterface cli =
                new CommandLineInterface(menuCommandRegistry, scanner);
        try {
            cli.run();
        } catch (Exception e) {
            System.out.println("Something went wrong.");
        }
    }

    private static Map<String, CommandHandler> initializeHandlers(
            Scanner scanner) {
        final WaterDiary waterDiary = new WaterDiary(new EntryStorage<>());
        final Map<String, CommandHandler> menuSupportedCommands = Map.of(
                //"create-exercise", new ExerciseCreationHandler(exerciseList),
                //"create-food", new FoodCreationHandler(foodList),
                //"log-meal", new MealLogHandler(foodList),
                //"register", new UserCreationHandler(),
                //"log-workout", new WorkoutLogHandler(exerciseList)
                "log-water", new WaterDiaryLogHandler(waterDiary, scanner),
                "search-water-log", new WaterDiarySearchHandler(waterDiary,
                        scanner)
        );
        return menuSupportedCommands;
    }
}
