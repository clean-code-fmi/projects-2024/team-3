package app.logging.food;
import java.util.HashMap;
import java.util.Map;

public final class FoodList {
    private HashMap<String, Food> foods;

    public FoodList() {
        foods = new HashMap<String, Food>();
    }

    /**
     * This function gets the food and will return null
     * if the food doesn't exist.
     */
    public Food getFood(String name) {
        for (Map.Entry<String, Food> food : foods.entrySet()) {
            if (food.getValue().getName().equalsIgnoreCase(name)) {
                return food.getValue();
            }
        }

        return null;
    }

    public boolean addFood(String name, Food food) {
        if (getFood(name) == null) {
            foods.put(name, food);
            return true;
        } else {
            return false;
        }
    }

}
