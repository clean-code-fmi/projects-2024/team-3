package app.logging.food;


import java.util.Objects;

/**
 * all values are per 100g.
 */


public final class Food {
    private String name;
    private String description;
    private double proteins;
    private double carbs;
    private double fats;

    public Food() {
        this("", "", 0, 0, 0);
    }

    public Food(String name,
         String description,
         double proteins,
         double carbs,
         double fats
    ) {
        this.name = name;
        this.description = description;
        this.carbs = carbs;
        this.proteins = proteins;
        this.fats = fats;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getCarbs() {
        return carbs;
    }

    public double getProteins() {
        return proteins;
    }

    public double getFats() {
        return fats;
    }

    /**
     * returned value is per 100g
     */
    public double getCalories() {
        return 4 * (carbs + proteins) + 9 * fats;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Food food = (Food) o;
        return Double.compare(getProteins(), food.getProteins()) == 0
            && Double.compare(getCarbs(), food.getCarbs()) == 0
            && Double.compare(getFats(), food.getFats()) == 0
            && Objects.equals(getName(), food.getName())
            && Objects.equals(getDescription(), food.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(),
                            getProteins(), getCarbs(), getFats());
    }
}
