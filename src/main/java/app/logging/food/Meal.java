package app.logging.food;

import java.util.HashMap;
import java.util.Map;

public final class Meal {
    public enum MealType { SNACK, BREAKFAST, LUNCH, DINNER, UNKNOWN };

    private HashMap<Food, Integer> mealItems;
    private MealType type;
    private double proteins;
    private double carbs;
    private double fats;
    private double calories;


    public Meal() {
        mealItems = new HashMap<Food, Integer>();
        type = MealType.UNKNOWN;
        proteins = 0;
        carbs = 0;
        fats = 0;
    }

    public void addToMeal(Food food, Integer quantityToAdd) {
        updateMacros(food, quantityToAdd);
        updateCalories(food, quantityToAdd);

        Integer quantity = getCurrentQuantity(food) + quantityToAdd;
        mealItems.put(food, quantity);
    }

    public void setType(MealType type) {
        this.type = type;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public HashMap<Food, Integer> getMealItems() {
        return this.mealItems;
    }

    public MealType getType() {
        return this.type;
    }

    public Double getProteins() {
        return this.proteins;
    }

    public Double getCarbs() {
        return this.carbs;
    }

    public Double getFats() {
        return this.fats;
    }

    public Double getCalories() {
        return this.calories;
    }

    public Integer getFoodQuantity(Food food) {
        return mealItems.get(food);
    }

    private void updateMacros(Food food, Integer quantity) {
        setProteins(proteins + food.getProteins() * quantity / 100);
        setCarbs(carbs + food.getCarbs() * quantity / 100);
        setFats(fats + food.getFats() * quantity / 100);
    }

    private void updateCalories(Food food, Integer quantity) {
        setCalories(getCalories() + food.getCalories() * quantity / 100);
    }

    private Integer getCurrentQuantity(Food food) {
        for (Map.Entry<Food, Integer> entry : mealItems.entrySet()) {
            if (entry.getKey().equals(food)) {
                return entry.getValue();
            }
        }
        return 0;
    }
}

