package app.logging.food;

import java.io.InputStream;
import java.util.Scanner;

public final class FoodMenu {
    private FoodList foodList;
    private Scanner scanner;

    public FoodMenu(InputStream inputStream, FoodList foodList) {
        this.foodList = foodList;
        scanner = new Scanner(inputStream);
    }

    public void createFood() {
        Food foodToBeAdded = new Food();


        System.out.print("Enter food name: ");
        foodToBeAdded.setName(scanner.nextLine());

        System.out.print("Enter description name: ");
        foodToBeAdded.setDescription(scanner.nextLine());

        System.out.print("Enter proteins amount: ");
        foodToBeAdded.setProteins(scanner.nextDouble());

        System.out.print("Enter carbs amount: ");
        foodToBeAdded.setCarbs(scanner.nextDouble());

        System.out.print("Enter fats amount: ");
        foodToBeAdded.setFats(scanner.nextDouble());

        foodList.addFood(foodToBeAdded.getName(), foodToBeAdded);

    }
}
