package app.logging.water;

public final class Water {
    private final double amount;

    public Water(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return amount + " ml.";
    }

}
