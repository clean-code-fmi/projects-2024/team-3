package app.logging.water;

import app.logging.EntryStorage;
import java.time.LocalDateTime;
import java.util.SortedMap;

public final class WaterDiary {
    private EntryStorage<Water> entries;

    public WaterDiary(EntryStorage<Water> entries) {
        this.entries = entries;
    }

    public void logWater(LocalDateTime timestamp, double amount) {
        Water toBeAdded = new Water(amount);
        entries.add(timestamp, toBeAdded);
    }

    public SortedMap<LocalDateTime, Water> getEntriesByDate(
            LocalDateTime date) {
        return entries.getEntriesByDate(date);
    }

    public SortedMap<LocalDateTime, Water> getEntriesByTimeFrame(
            LocalDateTime from, LocalDateTime to) {
        return entries.getEntriesByTimeFrame(from, to);
    }

    public SortedMap<LocalDateTime, Water> getAllEntries() {
        return entries.getAllEntries();
    }
}
