package app.logging.workout;

import java.util.ArrayList;

import app.logging.exercise.ExerciseEntry;
import app.logging.exercise.Exercise;

public class Workout {
    private ArrayList<ExerciseEntry> workoutSetEntries;

    public Workout() {
        workoutSetEntries = new ArrayList<ExerciseEntry>();
    }

    public final ArrayList<ExerciseEntry> getWorkoutSetEntries() {
        return workoutSetEntries;
    }

    public final void logExercise(Exercise exercise, double duration) {

        workoutSetEntries.add(new ExerciseEntry(exercise, duration));
    }

    public final double currentWorkoutCalories() {
        double workoutCalories = 0;
        for (ExerciseEntry entry : workoutSetEntries) {
            workoutCalories += entry.calculateCaloriesBurned();
        }
        return workoutCalories;
    }

    private double calculateWorkoutLength() {
        double workoutLength = 0;
        for (ExerciseEntry exerciseEntry : workoutSetEntries) {
            workoutLength += exerciseEntry.duration();
        }
        return workoutLength;
    }
}
