package app.logging.workout;


import app.logging.exercise.Exercise;
import app.logging.exercise.ExerciseEntry;
import app.logging.exercise.ExerciseList;

import java.io.InputStream;
import java.util.Scanner;

public class WorkoutMenu {
    private Scanner scanner;
    private ExerciseList exerciseList;

    public WorkoutMenu(InputStream iStr) {
        scanner = new Scanner(iStr);
    }

    public WorkoutMenu(InputStream iStr, ExerciseList exerciseList) {
        scanner = new Scanner(iStr);
        this.exerciseList = exerciseList;
    }

    public final void logExerciseFromStream(Workout workout) {
        System.out.println("-- Logging an exercise to workout --");
        System.out.print("Enter exercise name: ");
        String exercise = scanner.nextLine();
        System.out.print("Enter duration/reps: ");
        double duration = scanner.nextDouble();
        scanner.nextLine(); // buffer for '\n'
        Exercise exerciseToAdd = exerciseList.searchExercise(exercise);
        workout.logExercise(exerciseToAdd, duration);
    }
    public final String printCurrentWorkout(Workout workout) {

        StringBuilder str = new StringBuilder();

        for (ExerciseEntry entry : workout.getWorkoutSetEntries()) {
            str.append(entry.exercise().getName()).append(" - ");
            str.append(entry.exercise().getDescription()).append('\n');
            str.append("Reps: ").append(entry.duration());
            str.append(", Burned calories: ")
                    .append(entry.calculateCaloriesBurned())
                    .append("kcal\n");
        }

        str.append("\nTotal calories burned: ");
        str.append(workout.currentWorkoutCalories()).append("kcal\n");

        return str.toString();
    }

}
