package app.logging;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

public final class EntryStorage<T> {
    private final TreeMap<LocalDateTime, T> entries;

    public EntryStorage() {
        this.entries = new TreeMap<>();
    }

    public void add(LocalDateTime timestamp, T entry) {
        entries.put(timestamp, entry);
    }

    public SortedMap<LocalDateTime, T> getEntriesByDate(
            LocalDateTime timestamp) {
        LocalDateTime startOfDay = timestamp.toLocalDate().atStartOfDay();
        LocalDateTime endOfDay = startOfDay.plusDays(1);

        return getEntrySubMapFromToDate(startOfDay, endOfDay);
    }

    public SortedMap<LocalDateTime, T> getEntriesByTimeFrame(
            LocalDateTime from, LocalDateTime to) {
        return getEntrySubMapFromToDate(from, to);
    }

    public SortedMap<LocalDateTime, T> getAllEntries() {
        return entries;
    }

    private SortedMap<LocalDateTime, T> getEntrySubMapFromToDate(
            LocalDateTime from, LocalDateTime to) {
        if (entries.isEmpty()) {
            throw new NoSuchElementException("No entries found within the "
                    + "specified time frame.");
        }
        if (from.isAfter(to)) {
            // restoreChronologicalOrder
            LocalDateTime temp = from;
            from = to;
            to = temp;
        }

        LocalDateTime firstDate = entries.firstKey();
        LocalDateTime lastDate = entries.lastKey();

        if (to.isBefore(firstDate) || from.isAfter(lastDate)) {
            throw new NoSuchElementException("No entries found within the "
                    + "specified time frame.");
        }

        from = from.isBefore(firstDate) ? firstDate : from;
        to = to.isAfter(lastDate) ? lastDate.plusSeconds(1) : to;

        SortedMap<LocalDateTime, T> subMap = entries.subMap(from, to);

        if (subMap.isEmpty()) {
            throw new NoSuchElementException("No entries found within the "
                    + "specified time frame.");
        }

        return subMap;
    }

}
