package app.logging.exercise;

import java.io.InputStream;
import java.util.Scanner;


public final class ExerciseMenu {
    private final Scanner scanner;
    private final ExerciseList exerciseList;

    public ExerciseMenu(Scanner scanner, ExerciseList exerciseList) {
        this.scanner = scanner;
        this.exerciseList = exerciseList;
    }

    public ExerciseMenu(InputStream iStr, ExerciseList exerciseList) {
        scanner = new Scanner(iStr);
        this.exerciseList = exerciseList;
    }

    public void createExercise() {
        System.out.print("Enter exercise name: ");
        String name = scanner.nextLine();

        System.out.print("Enter calories burned per unit time: ");
        int caloriesBurned = Integer.parseInt(scanner.nextLine());

        System.out.println("Enter exercise description:");
        String description = inputMultiLineString();

        exerciseList.addExercise(
                new Exercise(name, description, caloriesBurned));
    }

    private String inputMultiLineString() {
        StringBuilder descriptionBuilder = new StringBuilder();

        String inputLine;
        while (true) {
            inputLine = scanner.nextLine();
            if (inputLine.isEmpty()) {
                break;
            }
            descriptionBuilder.append(inputLine).append("\n");
        }

        return descriptionBuilder.toString();
    }

}
