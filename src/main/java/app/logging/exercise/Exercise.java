package app.logging.exercise;

public final class Exercise {
    private String exerciseName;

    private String description;

    private int caloriesBurnedPerUnitTime;


    public String getName() {
        return exerciseName;
    }

    public void setName(String name) {
        this.exerciseName = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCaloriesBurnedPerUnitTime() {
        return caloriesBurnedPerUnitTime;
    }

    public void setCaloriesBurnedPerUnitTime(int calories) {
        this.caloriesBurnedPerUnitTime = calories;
    }

    public Exercise() {
    }

    public Exercise(String name, String description, int caloriesPerUnitTime) {
        this.exerciseName = name;
        this.description = description;
        this.caloriesBurnedPerUnitTime = caloriesPerUnitTime;
    }

    @Override
    public String toString() {
        return "Information about exercise: \n"
                + "Exercise name: "
                + this.exerciseName
                + "\n"
                + "Exercise description: "
                + this.description
                + "\n"
                + "Exercise Calories Burned: "
                + this.caloriesBurnedPerUnitTime;
    }
}

