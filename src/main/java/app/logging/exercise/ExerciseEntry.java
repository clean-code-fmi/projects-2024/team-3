package app.logging.exercise;

public record ExerciseEntry(Exercise exercise, double duration) {

    public double calculateCaloriesBurned() {
        return exercise.getCaloriesBurnedPerUnitTime() * this.duration;
    }
}
