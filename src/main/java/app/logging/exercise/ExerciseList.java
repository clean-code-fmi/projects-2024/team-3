package app.logging.exercise;

import java.util.HashMap;
import java.util.Map;

public final class ExerciseList {
    private final HashMap<Integer, Exercise> exercises =
            new HashMap<>();

    public void addExercise(Exercise exercise) {
        int key = exercises.size() + 1;
        if (exerciseExists(exercise.getName())) {
            throw new RuntimeException("Exercise already exists");
        }
        exercises.put(key, exercise);
    }

    public Exercise searchExercise(String exerciseName) {
        for (Map.Entry<Integer, Exercise> exercise : exercises.entrySet()) {
            if (exercise.getValue().getName()
                    .equalsIgnoreCase(exerciseName)) {
                return exercise.getValue();
            }
        }
        return null;
    }

    public boolean exerciseExists(String exerciseName) {
        return searchExercise(exerciseName) != null;
    }

}
