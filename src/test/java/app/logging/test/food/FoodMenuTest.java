package app.logging.test.food;

import app.logging.food.Food;
import app.logging.food.FoodList;
import app.logging.food.FoodMenu;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.io.InputStream;


public class FoodMenuTest {

    @Test
    void expectCreateFoodSuccess() {
        String input = "apple\nfruit\n0.5\n30\n1";

        InputStream stream = new ByteArrayInputStream(input.getBytes());

        FoodList mockFoodList = Mockito.mock(FoodList.class);

        FoodMenu foodMenu = new FoodMenu(stream, mockFoodList);

        Food expectedFood = new Food("apple",
                                 "fruit",
                                   0.5,
                                     30,
                                      1);

        foodMenu.createFood();

        verify(mockFoodList).addFood(expectedFood.getName(), expectedFood);
    }
}
