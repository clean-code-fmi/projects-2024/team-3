package app.logging.test.food;

import app.logging.food.Food;
import app.logging.food.FoodList;
import app.logging.food.Meal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MealTest {
    private static final Food BANANA = new Food("banana",
                                                "fruit",
                                                0.5,
                                                30,
                                                1);

    private static final Food CAKE = new Food("cake",
                                              "dessert",
                                              3,
                                              70,
                                              15);

    @Test
    public void expectWhenAddingItemsToMealItemsToBeThere() {
        Meal snack = new Meal();

        assertTrue(snack.getMealItems().isEmpty());
        snack.addToMeal(BANANA, 110);
        assertFalse(snack.getMealItems().isEmpty());

        snack.addToMeal(CAKE, 200);

        assertEquals(110, snack.getFoodQuantity(BANANA));
        assertEquals(200, snack.getFoodQuantity(CAKE));

    }


    @Test
    public void expectGetCaloriesToBeCorrect() {
        FoodList foodList = new FoodList();

        foodList.addFood("banana", BANANA);
        foodList.addFood("cake", CAKE);

        Meal breakfast = new Meal();

        breakfast.addToMeal(foodList.getFood("banana"), 140);
        breakfast.addToMeal(foodList.getFood("cake"), 70);

        double expectedCalories = BANANA.getCalories() * 140 / 100
                                + CAKE.getCalories() * 70 / 100;

        assertEquals(expectedCalories, breakfast.getCalories());
    }

    @Test
    public void expectGetCaloriesCorrectWhenAddingFoodTwice() {
        Meal testMeal = new Meal();

        testMeal.addToMeal(BANANA, 100);
        testMeal.addToMeal(BANANA, 100);

        assertEquals(2 * BANANA.getCalories(), testMeal.getCalories());
    }

    @Test
    public void expectUpdateMacrosCorrectWhenAddingFoodTwice() {
        Meal testMeal = new Meal();

        testMeal.addToMeal(BANANA, 100);
        assertEquals(0.5, testMeal.getProteins());

        testMeal.addToMeal(BANANA, 200);
        assertEquals(1.5, testMeal.getProteins());
    }

    @Test
    public void expectAddToMealToUpdateMacros() {
        Meal testMeal = new Meal();

        assertEquals(0, testMeal.getProteins());
        assertEquals(0, testMeal.getCarbs());
        assertEquals(0, testMeal.getFats());

        Integer quantity = 140;
        testMeal.addToMeal(BANANA, quantity);

        double expectedProteins = BANANA.getProteins() * quantity / 100;
        assertEquals(expectedProteins, testMeal.getProteins());

        quantity = 110;
        testMeal.addToMeal(CAKE, quantity);

        expectedProteins += CAKE.getProteins() * quantity / 100;
        assertEquals(expectedProteins, testMeal.getProteins());
    }
}
