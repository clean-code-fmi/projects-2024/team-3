package app.logging.test.food;

import app.logging.food.Food;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FoodTest {

    @Test
    void whenAddingFoodDataExpectItToBeSaved() {
        Food apple = new Food("apple", "fruit", 0.5, 30, 0.1);
        assertEquals(30, apple.getCarbs());
        assertEquals(0.5, apple.getProteins());
        assertEquals(0.1, apple.getFats());
        assertEquals("apple", apple.getName());
        assertEquals("fruit", apple.getDescription());

        apple.setDescription("goodFruit");
        assertEquals("goodFruit", apple.getDescription());

        apple.setProteins(0.2);
        assertEquals(0.2, apple.getProteins());

        apple.setCarbs(25);
        assertEquals(25, apple.getCarbs());

        apple.setFats(0.4);
        assertEquals(0.4, apple.getFats());

        double caloriesFormula = 4 * apple.getCarbs()
                              +  4 * apple.getProteins()
                              +  9 * apple.getFats();
        assertEquals(caloriesFormula, apple.getCalories());
    }




}
