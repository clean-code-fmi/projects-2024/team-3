package app.logging.test.food;
import app.logging.food.Food;
import app.logging.food.FoodList;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class FoodListTest {


    @Test
    void whenAddingFoodToTheListExpectItToBeThere() {
        Food banana = new Food("banana", "fruit", 1, 54, 0.2);
        FoodList foodList = new FoodList();

        foodList.addFood("banana", banana);


        assertEquals(banana, foodList.getFood("banana"));
    }

    @Test
    void whenAddingFoodLeadsToDuplicationExpectAddingToFail() {
        Food banana = new Food("banana", "fruit", 1, 54, 0.2);
        FoodList foodList = new FoodList();

        foodList.addFood("banana", banana);


        Food bAnana = new Food("bAnana", "fruite", 11, 541, 1.2);

        assertFalse(foodList.addFood("bAnana", bAnana));
        assertEquals(banana, foodList.getFood("bAnana"));
    }

}




