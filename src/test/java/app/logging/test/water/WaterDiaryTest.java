package app.logging.test.water;

import app.logging.EntryStorage;
import app.logging.water.Water;
import app.logging.water.WaterDiary;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.SortedMap;

import static app.logging.date.DateConverter.convert;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WaterDiaryTest {

    @Test
    void waterLogEntryCreation() {
        Water entry = new Water(0.5);
        double delta = 0.001;
        assertEquals(0.5, entry.getAmount(), delta);
    }

    @Test
    void waterDiaryLogWater() {
        WaterDiary waterDiary = new WaterDiary(new EntryStorage<>());
        LocalDateTime currentTime = LocalDateTime.now();
        waterDiary.logWater(currentTime, 1.0);
        SortedMap<LocalDateTime, Water> waterLog = waterDiary.getAllEntries();
        double delta = 0.001;
        assertEquals(1, waterLog.size());
        assertEquals(1.0, waterLog.get(currentTime).getAmount(),
                delta);
    }

    @Test
    void waterDiaryGetEntries() {
        WaterDiary waterDiary = new WaterDiary(new EntryStorage<>());
        LocalDateTime currentTime = LocalDateTime.now();
        waterDiary.logWater(currentTime, 0.5);
        waterDiary.logWater(currentTime.plusMinutes(2), 1.5);
        SortedMap<LocalDateTime, Water> waterLog = waterDiary.getAllEntries();
        double delta = 0.001;
        assertEquals(2, waterLog.size());
        assertEquals(0.5, waterLog.get(currentTime).getAmount(),
                delta);
        assertEquals(1.5, waterLog.get(currentTime.plusMinutes(2))
                .getAmount(), delta);
    }

    @Test
    void waterDiaryGetEntriesFromTimeFrame() {
        WaterDiary waterDiary = new WaterDiary(new EntryStorage<>());
        LocalDateTime timestamp1 = convert(
                "10-10-2024 11:30");
        LocalDateTime timestamp2 = convert(
                "10-10-2024 21:30");
        for (int i = 1; i <= 23; i++) {
            String stringTimeStamp;
            if (i < 10) {
                stringTimeStamp = String.format("10-10-2024 0" + i + ":30");
            } else {
                stringTimeStamp = String.format("10-10-2024 " + i + ":00");
            }
            LocalDateTime entryTimeStamp =
                    convert(stringTimeStamp);
            waterDiary.logWater(entryTimeStamp, i);
        }
        SortedMap<LocalDateTime, Water> waterLog = waterDiary.
                getEntriesByTimeFrame(timestamp1, timestamp2);
        assertEquals(10, waterLog.size());
    }

    @Test
    void waterDiaryGetEntriesFromDate() {
        WaterDiary waterDiary = new WaterDiary(new EntryStorage<>());
        LocalDateTime timestamp1 = convert(
                "10-10-2024 11:30");
        for (int i = 1; i <= 23; i++) {
            String stringTimeStamp;
            if (i < 10) {
                stringTimeStamp = String.format("10-10-2024 0" + i + ":30");
            } else {
                stringTimeStamp = String.format("10-10-2024 " + i + ":00");
            }
            LocalDateTime entryTimeStamp =
                    convert(stringTimeStamp);
            waterDiary.logWater(entryTimeStamp, i);
        }
        SortedMap<LocalDateTime, Water> waterLog = waterDiary.
                getEntriesByDate(timestamp1);
        assertEquals(23, waterLog.size());
    }
}
