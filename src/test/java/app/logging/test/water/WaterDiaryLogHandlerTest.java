package app.logging.test.water;

import app.cli.handler.WaterDiaryLogHandler;
import app.logging.water.WaterDiary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public final class WaterDiaryLogHandlerTest {
    private WaterDiary waterDiary;

    private WaterDiaryLogHandler waterLogHandler;
    private Scanner scanner;

    private final ByteArrayOutputStream outContent =
            new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        waterDiary = mock(WaterDiary.class);
        System.setOut(new PrintStream(outContent));
        scanner = mock(Scanner.class);
        waterLogHandler = new WaterDiaryLogHandler(waterDiary, scanner);
    }

    @Test
    public void testExecuteValidInput() {
        when(scanner.nextLine()).thenReturn("250");

        waterLogHandler.execute();

        verify(waterDiary).logWater(any(LocalDateTime.class), eq(250.0));
        assertTrue(outContent.toString().
                contains("Water was logged successfully!"));
    }

    @Test
    public void testExecuteInvalidInput() {
        when(scanner.nextLine()).thenReturn("abc").thenReturn("250");

        waterLogHandler.execute();

        assertTrue(outContent.toString().contains("Invalid input. "
                + "Please enter a numeric value."));
    }

    @Test
    public void testDescriptionToString() {
        String description = waterLogHandler.getDescription();
        assertEquals("Allows the user to log their water intake.",
                description);
    }

}
