package app.logging.test.water;

import app.cli.handler.WaterDiarySearchHandler;
import app.logging.water.Water;
import app.logging.water.WaterDiary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class WaterDiarySearchHandlerTest {
    private WaterDiary waterDiary;
    private WaterDiarySearchHandler searchHandler;
    private Scanner scanner;
    private final ByteArrayOutputStream outContent =
            new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        waterDiary = mock(WaterDiary.class);
        System.setOut(new PrintStream(outContent));
        scanner = mock(Scanner.class);
        searchHandler = new WaterDiarySearchHandler(waterDiary, scanner);
    }

    @Test
    public void testExecuteSearchByDateValid() {

        when(scanner.nextLine()).thenReturn("date").thenReturn("01-01-2023");
        LocalDateTime date = LocalDateTime.of(2023, 1, 1, 0, 0);
        SortedMap<LocalDateTime, Water> entries = new TreeMap<>();
        entries.put(date, new Water(250));
        when(waterDiary.getEntriesByDate(date)).thenReturn(entries);

        searchHandler.execute();

        assertTrue(outContent.toString().contains("01-01-2023 00:00: 250"));
    }

    @Test
    public void testExecuteSearchByDateInvalid() {
        when(scanner.nextLine()).thenReturn("date")
                .thenReturn("invalid-date");

        searchHandler.execute();

        assertTrue(outContent.toString().contains("Invalid date format.\n"
                + "Please use DD-MM-YYYY or DD-MM-YYYY HH:mm"));
    }

    @Test
    public void testExecuteSearchByTimeFrameValid() {
        when(scanner.nextLine()).thenReturn("timeframe")
                .thenReturn("01-01-2023 00:00")
                .thenReturn("02-01-2023 00:00");
        LocalDateTime from = LocalDateTime.of(2023, 1, 1, 0, 0);
        LocalDateTime to = LocalDateTime.of(2023, 1, 2, 0, 0);
        SortedMap<LocalDateTime, Water> entries = new TreeMap<>();
        entries.put(from, new Water(250));
        when(waterDiary.getEntriesByTimeFrame(from, to)).thenReturn(entries);

        searchHandler.execute();

        assertTrue(outContent.toString().contains("01-01-2023 00:00: 250"));
    }

    @Test
    public void testExecuteSearchByTimeFrameInvalid() {
        when(scanner.nextLine()).thenReturn("timeframe")
                .thenReturn("invalid-date").thenReturn("01-01-2023 00:00");

        searchHandler.execute();

        assertTrue(outContent.toString().contains("Invalid date format.\n"
                + "Please use DD-MM-YYYY or DD-MM-YYYY HH:mm"));
    }

    @Test
    public void testInvalidChoice() {
        when(scanner.nextLine()).thenReturn("invalid-choice");

        searchHandler.execute();

        assertTrue(outContent.toString().contains("Invalid input. "
                + "Please enter a valid choice."));
    }

    @Test
    public void testDescriptionToString() {
        String description = searchHandler.getDescription();
        assertTrue(description.contains("Allows you to search logs in "
                + "your water diary."));
    }
}
