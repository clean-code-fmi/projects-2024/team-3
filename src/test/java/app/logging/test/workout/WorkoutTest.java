/*package app.logging.test.workout;

import app.logging.exercise.Exercise;
import app.logging.workout.Workout;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorkoutTest {

    @Test
    void test1CreatingWorkout() {

        Workout workout = new Workout();

        assertEquals(0, workout.getWorkoutSetEntries().size());

        HashMap<Integer, Exercise> myMap = new HashMap<Integer, Exercise>();
        assertEquals(0, myMap.size());
    }
    @Test
    void test2GettersAddingLogging() {
        Exercise exercise1 = new Exercise("Running5km",
                "running 5km at pace 4:40",
                500);
        Exercise exercise2 = new Exercise("Pullups",
                "normal pullups",
                4);
        Workout workout = new Workout();

        // Pushups are not added in the logger yet
        // this way the logExercise function should do noting
        // but display an error message

        assertEquals(3, workout.getWorkoutSetEntries().size());
        assertEquals(660, workout.currentWorkoutCalories());

        Exercise exercise3 = new Exercise("Pushups",
                "normal pushups",
                3);
        workout.logExercise(exercise3, 1);

        assertEquals(5, workout.getWorkoutSetEntries().size());
        assertEquals(760, workout.currentWorkoutCalories());
    }
    @Test
    void test3GettersAddingLogging() {
        // this test tests what happens when the user
        // adds exercises with same names.
        // NOTE (adding not logging) !!
        // It's like the previous test but
        // added lines here and there

        Exercise exercise1 = new Exercise("Running5km",
                "running 5km at pace 4:40",
                500);
        Exercise exercise2 = new Exercise("Pullups",
                "normal pullups",
                4);
        // !!!!!!! - its like exercise2
        Exercise exerciseStar = new Exercise("Pullups",
                "reverse grip pullups",
                4);

        Workout workout = new Workout();

        workout.logExercise(exercise1, 1);
        workout.logExercise(exercise1, 1); // !!!!!!!
        workout.logExercise(exercise2, 1);
        workout.logExercise(exercise1, 1); // !!!!!!!
        workout.logExercise(exercise2, 1); // !!!!!!!
        workout.logExercise(exerciseStar, 1); // !!!!!!!

        // Pushups are not added in the logger yet
        // this way the logExercise function should do noting
        // but display an error message

        assertEquals(3, workout.getWorkoutSetEntries().size());
        assertEquals(660, workout.currentWorkoutCalories());

        Exercise exercise3 = new Exercise("Pushups",
                "normal pushups",
                3);
        workout.logExercise(exercise3, 1);
        workout.logExercise(exercise1, 1); // !!!!!!!
        workout.logExercise(exerciseStar, 1); // !!!!!!!
        workout.logExercise(exercise3, 1); // !!!!!!!

        assertEquals(5, workout.getWorkoutSetEntries().size());
        assertEquals(760, workout.currentWorkoutCalories());


    }
    @Test
    void test4WorkoutCalories() {
        Exercise exercise1 = new Exercise("Running",
                "running 1 hour",
                1000);
        Exercise exercise2 = new Exercise("Pushups",
                "normal pullups",
                4);
        Workout workout = new Workout();
        workout.logExercise(exercise1, 1);
        workout.logExercise(exercise2, 1);

        assertEquals(950, workout.currentWorkoutCalories());
    }


}
*/
