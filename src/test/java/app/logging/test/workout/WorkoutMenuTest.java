/*package app.logging.test.workout;

import app.logging.exercise.Exercise;
import app.logging.exercise.ExerciseList;
import org.junit.jupiter.api.Test;

import app.logging.workout.WorkoutMenu;
import app.logging.workout.Workout;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class WorkoutMenuTest {
    @Test
    void test2AddingExerciseFromStream() {
        String input = "Running\n1\n1000\n";
        InputStream stream = new ByteArrayInputStream(input.getBytes());
        ExerciseList list = new ExerciseList();


        Workout workout = new Workout();
        list.addExercise(new Exercise("Running", "ez nubs", 213));
        WorkoutMenu localUserMenu = new WorkoutMenu(stream, list);
        localUserMenu.logExerciseFromStream(workout);
    }
    @Test
    void test3AddingExerciseFromStream() {
        String input = "Running\n1\n1000\n"
                + "Pushups\nnormalpushups\n3\n"
                + "Pullups\nnormalpullups\n4\n";
        InputStream stream = new ByteArrayInputStream(input.getBytes());
        ExerciseList list = new ExerciseList();

        WorkoutMenu localUserMenu = new WorkoutMenu(stream);

        Workout workout = new Workout();
        list.addExercise(new Exercise("Running", "ez nubs", 213));

        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);

    }
    @Test
    void test4AddingExerciseFromStream() {
        // this is like previous test but test
        // what happens when adding two exercises
        // with same name
        String input = "Running\n1hour\n1000\n"
                + "Pushups\nnormalpushups\n3\n"
                + "Pushups\ndimondpushups\n3\n" // !!!
                + "Pullups\nnormalpullups\n4\n"
                + "Running\nSprint200m\n85\n"; // !!!
        InputStream stream = new ByteArrayInputStream(input.getBytes());

        WorkoutMenu localUserMenu = new WorkoutMenu(stream);
        Workout workout = new Workout();

        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout); // !!!
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout); // !!!

        assertEquals(0, workout.getWorkoutSetEntries().size());
    }
    @Test
    void test5LoggingExerciseFromStream() {
        String input = "Running\n1hour\n1000\n" // adding
                + "Pushups\nnormalpushups\n3\n" //adding
                + "Pullups\nnormalpullups\n4\n" //adding
                + "Running\n0.5\n" // logging
                + "Pushups\n30.5\n" //logging
                + "Pullups\n20\n" // logging
                + "Running\n0.6\n"; //logging
        InputStream stream = new ByteArrayInputStream(input.getBytes());

        WorkoutMenu localUserMenu = new WorkoutMenu(stream);
        Workout workout = new Workout();

        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);


        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);

        assertEquals(4, workout.getWorkoutSetEntries().size());
        assertEquals(1271.5, workout.currentWorkoutCalories());


    }
    @Test
    void test6PrintingCurrentWorkout() {
        // same as in test5

        String input = "Running\n1hour\n1000\n" // adding
                + "Pushups\nnormal pushups\n3\n" //adding
                + "Pullups\nnormal pullups\n4\n" //adding
                + "Running\n0.5\n" // logging
                + "Pushups\n30\n" //logging
                + "Pullups\n20\n" // logging
                + "Running\n0.6\n"; //logging
        InputStream stream = new ByteArrayInputStream(input.getBytes());

        WorkoutMenu localUserMenu = new WorkoutMenu(stream);
        Workout workout = new Workout();

        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);

        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        localUserMenu.logExerciseFromStream(workout);
        String str = "Running - 1hour\n"
                + "Reps: 0.5, Burned calories: 500.0kcal\n"
                + "Pushups - normal pushups\n"
                + "Reps: 30.0, Burned calories: 90.0kcal\n"
                + "Pullups - normal pullups\n"
                + "Reps: 20.0, Burned calories: 80.0kcal\n"
                + "Running - 1hour\n"
                + "Reps: 0.6, Burned calories: 600.0kcal\n\n"
                + "Total calories burned: 1270.0kcal\n";

        assertEquals(str, localUserMenu.printCurrentWorkout(workout));
    }
}
*/
