package app.logging.test.exercise;

import app.logging.exercise.Exercise;
import app.logging.exercise.ExerciseList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ExerciseListTest {

    @Test
    public void testAddExerciseAndLogExercise() {
        ExerciseList logger = new ExerciseList();

        Exercise existingExercise = new Exercise("Running", "Jogging", 100);
        logger.addExercise(existingExercise);

        Exercise foundExercise = logger.searchExercise("Running");

        // Exercise logged successfully
        assertEquals("Running", foundExercise.getName());
    }

    @Test
    public void testSearchExercise() {
        ExerciseList logger = new ExerciseList();

        Exercise exercise = new Exercise("Running", "Jogging", 100);
        logger.addExercise(exercise);

        Exercise foundExercise = logger.searchExercise(exercise.getName());

        assertEquals("Running", foundExercise.getName());
        assertEquals("Jogging", foundExercise.getDescription());
        assertEquals(100, foundExercise.getCaloriesBurnedPerUnitTime());
    }

    @Test
    public void testSearchExerciseNotFound() {
        ExerciseList logger = new ExerciseList();

        Exercise foundExercise = logger.searchExercise("Swimming");

        assertNull(foundExercise);
    }
}
