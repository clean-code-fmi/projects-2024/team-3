package app.logging.test.exercise;

import app.logging.exercise.Exercise;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExerciseTest {

    @Test
    public void testExerciseCreation() {
        Exercise exercise = new Exercise("Running", "Jogging", 100);

        assertEquals("Running", exercise.getName());
        assertEquals("Jogging", exercise.getDescription());
        assertEquals(100, exercise.getCaloriesBurnedPerUnitTime());
    }
}
