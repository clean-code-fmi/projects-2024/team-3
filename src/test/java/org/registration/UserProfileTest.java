package org.registration;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
public class UserProfileTest {

    @Test
    void testGreetSettersGender() {
        UserProfile myReg = new UserProfile();
        myReg.setFirstName("Ivan");
        myReg.setLastName("Draganov");
        myReg.setAge(33);
        myReg.setGender(Gender.Man);
        myReg.setEmail("ivanchoouu@abv.bg");
        myReg.setHeight(180);
        myReg.setWeight(90);
        myReg.setBodyType("Snazen");
        myReg.setGoalType("Bulking");

        String s = "! Welcome to our program !" + "\n"
                + myReg.getFirstName() + " " + myReg.getLastName() + "\n"
                + "Age: " + myReg.getAge() + ", Gender: "
                + "Man"
                + ", Height: " + myReg.getHeight() + "cm ,"
                + "Weight: " + myReg.getWeight() + "kg \n"
                + "BodyType: " + myReg.getBodyType() + ", Goal:"
                + myReg.getGoalType() + "\n" + "email: "
                + myReg.getEmail() + "\n";

        // Testing genderToString
        assertEquals(s, myReg.greet());
        assertEquals("Man", myReg.genderToString());
        myReg.setAge(10);
        assertEquals("Boy", myReg.genderToString());
        myReg.setGender(Gender.Woman);
        assertEquals("Girl", myReg.genderToString());
        myReg.setAge(33);
        assertEquals("Woman", myReg.genderToString());
    }
    @Test
    void testGettersSettersGreet() {
        UserProfile myReg = new UserProfile();
        myReg.setFirstName("Tsveta");
        myReg.setLastName("Pavlova");
        myReg.setAge(16);
        myReg.setGender(Gender.Woman);
        myReg.setEmail("tsveti12345@gmail.com");
        myReg.setHeight(155);
        myReg.setWeight(52);
        myReg.setBodyType("apple");
        myReg.setGoalType("no");

        String s = "! Welcome to our program !" + "\n"
                + myReg.getFirstName() + " " + myReg.getLastName() + "\n"
                + "Age: " + myReg.getAge() + ", Gender: "
                + "Girl"
                + ", Height: " + myReg.getHeight() + "cm ,"
                + "Weight: " + myReg.getWeight() + "kg \n"
                + "BodyType: " + myReg.getBodyType() + ", Goal:"
                + myReg.getGoalType() + "\n" + "email: "
                + myReg.getEmail() + "\n";

        assertEquals(s, myReg.greet());
        assertEquals("Tsveta", myReg.getFirstName());
        assertEquals("Pavlova", myReg.getLastName());
        assertEquals(16, myReg.getAge());
        assertEquals(Gender.Woman, myReg.getGender());
        assertEquals("tsveti12345@gmail.com", myReg.getEmail());
        assertEquals(155, myReg.getHeight());
        assertEquals(52, myReg.getWeight());
        assertEquals("apple", myReg.getBodyType());
        assertEquals("no", myReg.getGoalType());
    }

    @Test
    void testDefConstructor() {

        UserProfile myReg = new UserProfile();
        assertEquals("", myReg.getFirstName());
        assertEquals("", myReg.getLastName());
        assertEquals(0, myReg.getAge());
        assertNull(myReg.getGender());
        assertEquals("", myReg.getEmail());
        assertEquals(0, myReg.getHeight());
        assertEquals(0, myReg.getWeight());
        assertEquals("", myReg.getBodyType());
        assertEquals("", myReg.getGoalType());
    }

}
