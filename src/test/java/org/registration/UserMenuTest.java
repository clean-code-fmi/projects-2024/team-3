package org.registration;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserMenuTest {

    @Test
    void test1Registration() {
        // the 3 after 42 (age) is for trying to enter
        // correct gender but fails, then correct gender 1
        String input = "Petkan\nPetkanov\n42\n3\n1\n"
                        + "peshoturboto@gmail.com\n176\n"
                        + "70.5\nAthletic\n20pullups\n";
        InputStream stream = new ByteArrayInputStream(input.getBytes());

        UserMenu localUserMenu = new UserMenu(stream);

        UserProfile usrPrf = localUserMenu.register();

        String s = "! Welcome to our program !" + "\n"
                + "Petkan" + " Petkanov" + "\n"
                + "Age: " + 42 + ", Gender: "
                + "Man"
                + ", Height: " + 176 + "cm ,"
                + "Weight: " + 70.5 + "kg \n"
                + "BodyType: " + "Athletic" + ", Goal:"
                + "20pullups" + "\n" + "email: "
                + "peshoturboto@gmail.com" + "\n";

        assertEquals(s, usrPrf.greet());

    }

    @Test
    void test2Registration() {
        String input = "Penka\nPenkova\n16\n0\ncrimsonMustang83@abv.bg\n"
                        + "160\n60\nHourglass\nsub25min-5km\n";
        InputStream stream = new ByteArrayInputStream(input.getBytes());

        UserMenu localUserMenu = new UserMenu(stream);

        UserProfile usrPrf = localUserMenu.register();

        String s = "! Welcome to our program !" + "\n"
                + "Penka" + " Penkova" + "\n"
                + "Age: " + 16 + ", Gender: "
                + "Girl"
                + ", Height: " + 160 + "cm ,"
                + "Weight: " + 60.0 + "kg \n"
                + "BodyType: " + "Hourglass" + ", Goal:"
                + "sub25min-5km" + "\n" + "email: "
                + "crimsonMustang83@abv.bg" + "\n";

        assertEquals(s, usrPrf.greet());

    }

}
